from django.urls import path

from .views import *

urlpatterns = [
    path('',home_view,name="home"),
    path('all_transactions',transactions,name="transactions"),
    path('select_customer',all_customers,name="all_customers"),
    path('select_customer/<int:id>',customer),
    path('transfer/',transfer,name="transfer"),

]

